package com.lkakulia.lecture_23

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.grid_item_layout.view.*

class RecyclerViewAdapter(
    private val grid: MutableList<GridItem>,
    private val customListener: CustomListener
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.grid_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return grid.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var gridItem: GridItem

        fun onBind() {
            gridItem = grid[adapterPosition]

            itemView.imageButton.setOnClickListener {
                customListener.onClick(adapterPosition)
            }

            if (gridItem.isClicked) {
                if (gridItem.represents == "X") {
                    itemView.imageButton.setImageResource(R.mipmap.baseline_clear_black_48)
                } else if (gridItem.represents == "O"){
                    itemView.imageButton.setImageResource(R.mipmap.baseline_exposure_zero_black_48)
                }
            }
        }
    }
}