package com.lkakulia.lecture_23

interface CustomListener {
    fun onClick(adapterPosition: Int)
}