package com.lkakulia.lecture_23

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.GridLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: RecyclerViewAdapter
    private val grid = mutableListOf<GridItem>()
    private val grid2D = mutableListOf<MutableList<GridItem>>()
    private var numCols = 0
    private var previous = "X"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //submit button listener
        numberColsButton.setOnClickListener {
            if (numberColsEditText.text.isNotEmpty()) {
                numCols = numberColsEditText.text.toString().toInt()
                if (numCols in 3..5) {
                    numberColsEditText.visibility = View.GONE
                    numberColsButton.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE

                    recyclerView.layoutManager = GridLayoutManager(this, numCols)

                    //populate grid list
                    for (i in 0 until (numCols * numCols)) {
                        grid.add(GridItem())
                    }

                    //adapter
                    adapter = RecyclerViewAdapter(grid, object : CustomListener {
                        override fun onClick(adapterPosition: Int) {
                            val gridItem = grid[adapterPosition]
                            var winner = ""
                            if (!gridItem.isClicked) {
                                if (previous == "X") {
                                    gridItem.represents = "O"
                                    gridItem.isClicked = true
                                    previous = "O"

                                    winner = checkForWinner(gridItem.represents, adapterPosition)
                                    if (winner != "") {
                                        Toast.makeText(this@MainActivity, winner, Toast.LENGTH_LONG).show()
                                        disableClicks()
                                    }
                                } else {
                                    gridItem.represents = "X"
                                    gridItem.isClicked = true
                                    previous = "X"

                                    winner = checkForWinner(gridItem.represents, adapterPosition)
                                    if (winner != "") {
                                        Toast.makeText(this@MainActivity, winner, Toast.LENGTH_LONG).show()
                                        disableClicks()
                                    }
                                }
                                adapter.notifyItemChanged(adapterPosition)
                            }
                        }
                    })
                    recyclerView.adapter = adapter
                }
                else {
                    Toast.makeText(this, "The number must range from 3 to 5", Toast.LENGTH_SHORT).show()
                }
            }
            else {
                Toast.makeText(this, "Please fill in the field", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun checkForWinner(represents: String, adapterPosition: Int): String {

        //coordinates
        var x = 0
        var y = 0

        //populate grid2D list
        for (i in 0 until numCols) {
            grid2D.add(mutableListOf<GridItem>())
        }

        //transform 1D to 2D
        for (i in 0 until numCols) {
            for (j in 0 until numCols) {
                grid2D[i].add(grid[(j * numCols) + i])
            }
        }

        //convert adapterPosition to x, y coordinates
        for (i in 0 until numCols) {
            for (j in 0 until numCols) {
                if (adapterPosition == ((j*numCols)+i)) {
                    x = j
                    y = i
                    break
                }
            }
        }

        //check for winner algorithm
        var counter = 0

        //check for columns
        for (i in 0 until numCols) {
            if (represents == grid2D[i][x].represents) {
                counter++
            }
            else {
                counter = 0
                break
            }
        }
        if (counter == numCols) {
            return "$represents has won in columns!"
        }

        //check for rows
        for (j in 0 until numCols) {
            if (represents == grid2D[y][j].represents) {
                counter++
            }
            else {
                counter = 0
                break
            }
        }
        if (counter == numCols) {
            return "$represents has won in rows!"
        }

        //check for first diagonal
        if (x == y) {
            for (i in 0 until numCols) {
                if (represents == grid2D[i][i].represents) {
                    counter++
                }
                else {
                    counter = 0
                    break
                }
            }
            if (counter == numCols) {
                return "$represents has won in first diagonal!"
            }
        }

        //check for second diagonal
        if ((x + y) == numCols - 1) {
            for (i in 0 until numCols) {
                if (represents == grid2D[i][numCols - 1 - i].represents) {
                    counter++
                }
                else {
                    counter = 0
                    break
                }
            }
            if (counter == numCols) {
                return "$represents has won in second diagonal!"
            }
        }

        //draw
        if (isFull()) {
            return "Draw!"
        }

        return ""
    }

    private fun disableClicks() {
        for (i in 0 until (numCols * numCols)) {
            grid[i].isClicked = true
        }
        adapter.notifyDataSetChanged()
    }

    private fun isFull(): Boolean {
        for (i in 0 until (numCols * numCols)) {
            if (!grid[i].isClicked) {
                return false
            }
        }
        return true
    }
}
